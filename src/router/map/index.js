import openlayer from './openlayer'
import mapbox from './mapbox'
import cesium from './cesium'
export default {
    path: 'gis',
    name: 'gis',
    component: () => import( '@/views/map/index.vue'),
    children: [
      openlayer,
      mapbox,
      cesium
    ]
}