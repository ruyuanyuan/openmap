export default{
  path: 'cesium',
  name: 'cesium',
  component: () => import('@/views/map/cesium/index.vue'),
  meta:{
    isLayout:true,
    isHide:false,
    title:"cesium"
  },
  children: [
    {
      path: 'maker',
      name: 'cesium-maker',
      meta:{
        isLayout:false,
        isHide:false,
        title:"登录"
      },
      component: () => import('@/views/home.vue'),
    },
  ]
}