export default {
  path: 'mapbox',
  name: 'mapbox',
  component:  ()=>import('@/views/map/mapbox/index.vue'),
  meta:{
    isLayout:true,
    isHide:false,
    title:"mapbox"
  },
  children: [
    {
      path: 'createMap',
      name: 'create-map',
      meta:{
        isHide:false,
        title:"create-map"
      },
      component: ()=>import('@/views/map/mapbox/basics/create-map.vue'),
    },
    {
      path: 'marker',
      name: 'marker',
      meta:{
        isHide:false,
        title:"marker"
      },
      component: ()=>import('@/views/map/mapbox/basics/marker.vue'),
    },
    {
      path: 'marker-geojson',
      name: 'marker-geojson',
      meta:{
        isHide:false,
        title:"marker-geojson"
      },
      component: ()=>import('@/views/map/mapbox/basics/marker-geojson.vue'),
    },
    {
      path: 'cluster',
      name: 'cluster',
      meta:{
        isHide:false,
        title:"cluster"
      },
      component: ()=>import('@/views/map/mapbox/basics/cluster.vue'),
    },
    {
      path: 'line',
      name: 'line',
      meta:{
        isHide:false,
        title:"line"
      },
      component: ()=>import('@/views/map/mapbox/basics/line.vue'),
    },
    {
      path: 'color-line',
      name: 'color-line',
      meta:{
        isHide:false,
        title:"line"
      },
      component: ()=>import('@/views/map/mapbox/basics/color-line.vue'),
    },
    {
      path: 'streamline',
      name: 'streamline',
      meta:{
        isHide:false,
        title:"line"
      },
      component: ()=>import('@/views/map/mapbox/basics/streamline.vue'),
    },
    {
      path: 'polygon',
      name: 'polygon',
      meta:{
        isHide:false,
        title:"polygon"
      },
      component: ()=>import('@/views/map/mapbox/basics/polygon.vue'),
    },
    {
      path: 'circle',
      name: 'circle',
      meta:{
        isHide:false,
        title:"circle"
      },
      component: ()=>import('@/views/map/mapbox/basics/circle.vue'),
    },
    {
      path: 'fill-extrusion',
      name: 'fill-extrusion',
      meta:{
        isHide:false,
        title:"fill-extrusion"
      },
      component: ()=>import('@/views/map/mapbox/basics/fill-extrusion.vue'),
    },
    {
      path: 'heatmap',
      name: 'heatmap',
      meta:{
        isHide:false,
        title:"heatmap"
      },
      component: ()=>import('@/views/map/mapbox/basics/heatmap.vue'),
    },
    {
      path: 'draw',
      name: 'draw',
      meta:{
        isHide:false,
        title:"draw"
      },
      component: ()=>import('@/views/map/mapbox/basics/draw.vue'),
    },
    {
      path: 'example/city',
      name: 'city',
      meta:{
        isHide:false,
        title:"city"
      },
      component: ()=>import('@/views/map/mapbox/example/city.vue'),
    },
  ]
}