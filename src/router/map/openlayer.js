export default {
  path: 'openlayer',
  name: 'openlayer',
  meta:{
    isLayout:true,
    isHide:false,
    title:"mapbox"
  },
  component: ()=>import('@/views/map/openlayer/index.vue'),
  children: [
    {
      path: 'createMap',
      name: 'createMap',
      meta:{
        isLayout:false,
        isHide:false,
        title:"创建地图"
      },
      component: ()=>import('@/views/map/openlayer/basics/create-map.vue')
    },
    {
      path: 'eagle-eye',
      name: 'eagle-eye',
      meta:{
        isLayout:false,
        isHide:false,
        title:"鹰眼地图"
      },
      component: ()=>import('@/views/map/openlayer/basics/eagle-eye.vue')
    },
    {
      path: 'export-img',
      name: 'export-img',
      meta:{
        isLayout:false,
        isHide:false,
        title:"导出图片"
      },
      component: ()=>import('@/views/map/openlayer/basics/export-img.vue')
    },
    {
      path: 'maker',
      name: 'openlayer-maker',
      meta:{
        isLayout:false,
        isHide:false,
        title:"Maker点"
      },
      component: ()=>import('@/views/map/openlayer/basics/maker.vue')
    },
    {
      path: 'line',
      name: 'openlayer-line',
      meta:{
        isLayout:false,
        isHide:false,
        title:"line线"
      },
      component: ()=>import('@/views/map/openlayer/basics/line.vue')
    },
    {
      path: 'square',
      name: 'openlayer-square',
      meta:{
        isLayout:false,
        isHide:false,
        title:"square正方形"
      },
      component: ()=>import('@/views/map/openlayer/basics/square.vue')
    },
    {
      path: 'rect',
      name: 'openlayer-rect',
      meta:{
        isLayout:false,
        isHide:false,
        title:"rect矩形"
      },
      component: ()=>import('@/views/map/openlayer/basics/rect.vue')
    },
    {
      path: 'circle',
      name: 'openlayer-circle',
      meta:{
        isLayout:false,
        isHide:false,
        title:"circle圆"
      },
      component: ()=>import('@/views/map/openlayer/basics/circle.vue')
    },
    {
      path: 'polygon',
      name: 'openlayer-polygon',
      meta:{
        isLayout:false,
        isHide:false,
        title:"多边形" 
      },
      component: ()=>import('@/views/map/openlayer/basics/polygon.vue')
    },
    
    {
      path: 'geojson',
      name: 'openlayer-geojson',
      meta:{
        isLayout:false,
        isHide:false,
        title:"GeoJSON"
      },
      component: ()=>import('@/views/map/openlayer/basics/geojson.vue')
    },
    {
      path: 'geojson-file',
      name: 'openlayer-geojson-file',
      meta:{
        isLayout:false,
        isHide:false,
        title:"GeoJSON-File"
      },
      component: ()=>import('@/views/map/openlayer/basics/geojson-file.vue')
    },
    {
      path: 'event',
      name: 'openlayer-event',
      meta:{
        isLayout:false,
        isHide:false,
        title:"地图事件"
      },
      component: ()=>import('@/views/map/openlayer/basics/event.vue')
    },
    // 示例
    {
      path: 'areaMask',
      name: 'openlayer-areaMask',
      meta:{
        isLayout:false,
        isHide:false,
        title:"区域高亮" 
      },
      component: ()=>import('@/views/map/openlayer/example/area-mask.vue')
    },
  ]
}