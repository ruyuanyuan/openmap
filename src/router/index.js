import {createRouter,createWebHashHistory} from 'vue-router'
import mapRouter from './map'
import canvasRouter from './canvas'
import threejsRouter from './threejs'
const routes = [
  {
    path: '/',
    name: 'index',
    component: () => import( /* webpackChunkName: "index" */ '@/views/index.vue'),
    redirect: '/home',
    meta:{
      isLayout:true,
      isHide:false
    },
    children: [
      {
        path: 'home',
        name: 'home',
        meta:{
          isLayout:false,
          isHide:false,
          title:"首页"
        },
        component: () => import( /* webpackChunkName: "home" */ '@/views/home.vue'),
      },
      mapRouter,
      canvasRouter,
      threejsRouter
    ]
  },
  {
    path: '/goodluck',
    name: 'goodluck',
    meta:{
      isLayout:false,
      isHide:true,
      title:"好运"
    },
    component: () => import( /* webpackChunkName: "goodluck" */ '@/views/good-luck.vue'),
  },
  {
    path: '/login',
    name: 'login',
    meta:{
      isLayout:false,
      isHide:true,
      title:"登录"
    },
    component: () => import( /* webpackChunkName: "login" */ '@/views/login.vue'),
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    meta:{
      isLayout:false,
      isHide:true,
      title:"404"
    },
    component: () => import( /* webpackChunkName: "NotFound" */ '@/views/404.vue'),
  },
]

const router = createRouter({
  routes,
  history: createWebHashHistory()
})
export default router