export default{
  path: 'threejs',
  name: 'threejs',
  component: () => import('@/views/threejs/index.vue'),
  meta:{
    isLayout:true,
    isHide:false,
    title:"threejs"
  },
  children: [
    {
      path: 'scene',
      name: 'scene',
      meta:{
        isLayout:false,
        isHide:false,
        title:"场景"
      },
      component: () => import('@/views/threejs/basic/scene.vue'),
    },
    {
      path: 'geometry',
      name: 'geometry',
      meta:{
        isLayout:false,
        isHide:false,
        title:"几何体"
      },
      component: () => import('@/views/threejs/basic/geometry.vue'),
    },
    {
      path: 'material',
      name: 'material',
      meta:{
        isLayout:false,
        isHide:false,
        title:"材质"
      },
      component: () => import('@/views/threejs/basic/material.vue'),
    },
    {
      path: 'light',
      name: 'light',
      meta:{
        isLayout:false,
        isHide:false,
        title:"灯光"
      },
      component: () => import('@/views/threejs/basic/light.vue'),
    },
    {
      path: 'annimation',
      name: 'annimation',
      meta:{
        isLayout:false,
        isHide:false,
        title:"动画"
      },
      component: () => import('@/views/threejs/basic/annimation.vue'),
    },
    {
      path: 'more-rect',
      name: 'more-rect',
      meta:{
        isLayout:false,
        isHide:false,
        title:"大量立方体"
      },
      component: () => import('@/views/threejs/basic/more-rect.vue'),
    },
    {
      path: 'rect-array',
      name: 'rect-array',
      meta:{
        isLayout:false,
        isHide:false,
        title:"正方体矩阵"
      },
      component: () => import('@/views/threejs/basic/rect-array.vue'),
    },
    {
      path: 'sawtooth',
      name: 'sawtooth',
      meta:{
        isLayout:false,
        isHide:false,
        title:"锯齿优化"
      },
      component: () => import('@/views/threejs/basic/sawtooth.vue'),
    },
    {
      path: 'dat-gui',
      name: 'dat-gui',
      meta:{
        isLayout:false,
        isHide:false,
        title:"GUI扩展库"
      },
      component: () => import('@/views/threejs/basic/dat-gui.vue'),
    },
    {
      path: 'points',
      name: 'points',
      meta:{
        isLayout:false,
        isHide:false,
        title:"点模型"
      },
      component: () => import('@/views/threejs/basic/points.vue'),
    },
    {
      path: 'lines',
      name: 'lines',
      meta:{
        isLayout:false,
        isHide:false,
        title:"线模型"
      },
      component: () => import('@/views/threejs/basic/lines.vue'),
    },
    {
      path: 'buffer',
      name: 'buffer',
      meta:{
        isLayout:false,
        isHide:false,
        title:"自定义几何体"
      },
      component: () => import('@/views/threejs/basic/buffer.vue'),
    },
    {
      path: 'transition',
      name: 'transition',
      meta:{
        isLayout:false,
        isHide:false,
        title:"几何体缩放平移旋转"
      },
      component: () => import('@/views/threejs/basic/transition.vue'),
    },
    {
      path: 'group',
      name: 'group',
      meta:{
        isLayout:false,
        isHide:false,
        title:"group层级模型"
      },
      component: () => import('@/views/threejs/basic/group.vue'),
    },
    {
      path: 'texture',
      name: 'texture',
      meta:{
        isLayout:false,
        isHide:false,
        title:"纹理贴图"
      },
      component: () => import('@/views/threejs/basic/texture.vue'),
    },
    {
      path: 'texture-array',
      name: 'texture-array',
      meta:{
        isLayout:false,
        isHide:false,
        title:"纹理贴图阵列"
      },
      component: () => import('@/views/threejs/basic/texture-array.vue'),
    },
    {
      path: 'grid-help',
      name: 'grid-help',
      meta:{
        isLayout:false,
        isHide:false,
        title:"纹理贴图阵列"
      },
      component: () => import('@/views/threejs/basic/grid-help.vue'),
    },
    {
      path: 'uv-annimation',
      name: 'uv-annimation',
      meta:{
        isLayout:false,
        isHide:false,
        title:"UV动画"
      },
      component: () => import('@/views/threejs/basic/uv-annimation.vue'),
    },
    {
      path: 'load-gltf',
      name: 'load-gltf',
      meta:{
        isLayout:false,
        isHide:false,
        title:"gltf模型加载"
      },
      component: () => import('@/views/threejs/basic/load-gltf.vue'),
    },
    {
      path: 'load-gltf-progress',
      name: 'load-gltf-progress',
      meta:{
        isLayout:false,
        isHide:false,
        title:"gltf模型加载进度"
      },
      component: () => import('@/views/threejs/basic/load-gltf-progress.vue'),
    }, 
    {
      path: 'PBR',
      name: 'PBR',
      meta:{
        isLayout:false,
        isHide:false,
        title:"PBR材质"
      },
      component: () => import('@/views/threejs/basic/PBR.vue'),
    },
    {
      path: 'saveimg',
      name: 'saveimg',
      meta:{
        isLayout:false,
        isHide:false,
        title:"保存图片"
      },
      component: () => import('@/views/threejs/basic/saveimg.vue'),
    },
    {
      path: 'raycaster',
      name: 'raycaster',
      meta:{
        isLayout:false,
        isHide:false,
        title:"点击事件"
      },
      component: () => import('@/views/threejs/basic/raycaster.vue'),
    },
    {
      path: 'edgesGeometry',
      name: 'edgesGeometry',
      meta:{
        isLayout:false,
        isHide:false,
        title:"边线模型"
      },
      component: () => import('@/views/threejs/basic/edgesGeometry.vue'),
    },
    {
      path: 'load-gltf-line',
      name: 'load-gltf-line',
      meta:{
        isLayout:false,
        isHide:false,
        title:"边线模型2"
      },
      component: () => import('@/views/threejs/basic/load-gltf-line.vue'),
    },
    {
      path: 'example/vr-room',
      name: 'vr-room',
      meta:{
        isLayout:false,
        isHide:false,
        title:"VR房间"
      },
      component: () => import('@/views/threejs/example/vr-room.vue'),
    },
    {
      path:'example/starry-sky',
      name: 'starry-sky',
      meta:{
        isLayout:false,
        isHide:false,
        title:"星空背景"
      },
      component: () => import('@/views/threejs/example/starry-sky.vue'),
    },
    {
      path:'example/map',
      name: 'map',
      meta:{
        isLayout:false,
        isHide:false,
        title:"3D地图"
      },
      component: () => import('@/views/threejs/example/map.vue'),
    }
  ]
}