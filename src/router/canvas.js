export default{
  path: 'canvas',
  name: 'canvas',
  component: () => import('@/views/canvas/index.vue'),
  meta:{
    isLayout:true,
    isHide:false,
    title:"canvas"
  },
  children: [
    {
      path: 'rect',
      name: 'canvas-rect',
      meta:{
        isLayout:false,
        isHide:false,
        title:"矩形"
      },
      component: () => import('@/views/canvas/rect.vue'),
    },
  ]
}