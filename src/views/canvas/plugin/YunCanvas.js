class YunCanvas {
  constructor(option){
    this.__bind()
    this.initCanvas(option)
  }
  initCanvas(option){
    this.canvas = document.getElementById(option.el)
    this.ctx = this.canvas.getContext('2d')
    let width = this.canvas.parentNode.offsetWidth;
    let height = this.canvas.parentNode.offsetHeight;
    this.canvas.width = width
    this.canvas.height = height
  }
  /**
   * 绘制矩形
   * @param {*} option 
   */
  drawRect(option){
    let {x,y,width,height,strokeColor,strokeWidth,fillColor} = option
    this.ctx.fillStyle = fillColor || 'yellow'
    this.ctx.strokeStyle = strokeColor || '#ff2d51'
    this.ctx.lineWidth  = strokeWidth || 2
    this.ctx.rect(x, y, width, height);
  }
  drawLine(){
    
  }
  /**
   * 绘制边框矩形
   * @param {*} option 
   */
  drawStrokeRect(option){
    let {x,y,width,height,strokeColor,strokeWidth,fillColor} = option
    this.ctx.fillStyle = fillColor || 'yellow'
    this.ctx.strokeStyle = strokeColor || '#ff2d51'
    this.ctx.lineWidth  = strokeWidth || 2
    this.ctx.strokeRect(x, y, width, height);
  }
  /**
   * 绘制填充矩形
   * @param {*} option 
   */
  drawFillRect(option){
    let {x,y,width,height,strokeColor,strokeWidth,fillColor} = option
    this.ctx.fillStyle = fillColor || 'yellow'
    this.ctx.strokeStyle = strokeColor || '#ff2d51'
    this.ctx.lineWidth  = strokeWidth || 2
    this.ctx.fillRect(x, y, width, height);
  }
  __bind() {
    const methods = Object.getOwnPropertyNames(Object.getPrototypeOf(this))
    methods.forEach(name => {
      if (typeof this[name] === 'function') {
        this[name] = this[name].bind(this)
      }
    })
  }
}
export default YunCanvas