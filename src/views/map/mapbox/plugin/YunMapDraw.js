import MapboxDraw from "@mapbox/mapbox-gl-draw";
import '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css'

//自由绘制
import PaintMode from "mapbox-gl-draw-paint-mode";
import DrawCircle  from 'mapbox-gl-draw-circle-mode';
class YunMapDraw {
  constructor(options) {
    this.setDrawStyle(options)
    this.map = options.map;
    this.draw = new MapboxDraw({
      displayControlsDefault: false,
      styles:this.styles,
      userProperties: true,
      controls:{
        point: true,
        line_string: true,
        polygon: true,
        trash: true,
        circle:true
      },
      modes:{
        ...MapboxDraw.modes,
        draw_paint_mode:PaintMode,
        draw_circle  : DrawCircle,
      },
      defaultMode:'draw_line_string'  //"direct_select" "draw_line_string" "draw_point" "draw_polygon" "static" "simple_select"
    });
    console.log(this.draw)
    this.map.addControl(this.draw);
    this.map.on('draw.create',()=>{
      options.onDrawStart&&options.onDrawStart()
    })
    this.map.on('draw.delete',()=>{
      options.onDrawDelete&&options.onDrawDelete()
    })
    this.map.on('draw.update',()=>{
      options.onDrawUpdate&&options.onDrawUpdate()
    })
    this.__bind();
  }
  initDraw() {

  }
  setDrawStyle(options) {
    this.styles = [
      {
        'id': 'highlight-active-points',
        'type': 'circle',
        'filter': ['all',
          ['==', '$type', 'Point'],
          ['==', 'meta', 'feature'],
          ['==', 'active', 'true']],
        'paint': {
          'circle-radius': 7,
          'circle-color': '#000000'
        }
      },
      {
        'id': 'points-are-blue',
        'type': 'circle',
        'filter': ['all',
          ['==', '$type', 'Point'],
          ['==', 'meta', 'feature'],
          ['==', 'active', 'false']],
        'paint': {
          'circle-radius': 5,
          'circle-color': '#000088'
        }
      },
      // line stroke
      {
        "id": "gl-draw-line",
        "type": "line",
        "filter": ["all", ["==", "$type", "LineString"]],
        "layout": {
          "line-cap": "round",
          "line-join": "round"
        },
        "paint": {
          "line-color": "#D20C0C",
          "line-dasharray": [0.2, 2],
          "line-width": 2
        }
      },
      // polygon fill
      {
        "id": "gl-draw-polygon-fill",
        "type": "fill",
        "filter": ["all", ["==", "$type", "Polygon"]],
        "paint": {
          "fill-color": "#D20C0C",
          "fill-outline-color": "#D20C0C",
          "fill-opacity": 0.1
        }
      },
      // polygon mid points
      {
        'id': 'gl-draw-polygon-midpoint',
        'type': 'circle',
        'filter': ['all',
          ['==', '$type', 'Point'],
          ['==', 'meta', 'midpoint']],
        'paint': {
          'circle-radius': 3,
          'circle-color': '#fbb03b'
        }
      },
      // polygon outline stroke
      // This doesn't style the first edge of the polygon, which uses the line stroke styling instead
      {
        "id": "gl-draw-polygon-stroke-active",
        "type": "line",
        "filter": ["all", ["==", "$type", "Polygon"]],
        "layout": {
          "line-cap": "round",
          "line-join": "round"
        },
        "paint": {
          "line-color": "#D20C0C",
          "line-dasharray": [0.2, 2],
          "line-width": 2
        }
      },
      // vertex point halos
      {
        "id": "gl-draw-polygon-and-line-vertex-halo-active",
        "type": "circle",
        "filter": ["all", ["==", "meta", "vertex"], ["==", "$type", "Point"]],
        "paint": {
          "circle-radius": 5,
          "circle-color": "#FFF"
        }
      },
      // vertex points
      {
        "id": "gl-draw-polygon-and-line-vertex-active",
        "type": "circle",
        "filter": ["all", ["==", "meta", "vertex"], ["==", "$type", "Point"]],
        "paint": {
          "circle-radius": 3,
          "circle-color": "#D20C0C",
        }
      }
    ]
  }
  setDrawMode(mode) {
    //"direct_select" "draw_line_string" "draw_point" "draw_polygon" "static" "simple_select" draw_paint_mode draw_circle
    this.draw.changeMode(mode)
  }
  drawGeojson(geojson) {
    this.draw.add(geojson)
  }
  __bind() {
    const methods = Object.getOwnPropertyNames(Object.getPrototypeOf(this))
    methods.forEach(name => {
      if (typeof this[name] === 'function') {
        this[name] = this[name].bind(this)
      }
    })
  }
}

export default YunMapDraw