import mapbox from "mapbox-gl";
import "mapbox-gl/dist/mapbox-gl.css";
mapbox.accessToken = 'pk.eyJ1IjoiZWNob2h5ZSIsImEiOiJjbGNuM3hmeXo2a2FjM3Fsa2JpM2praGp4In0.olt3jKXnWYuYiDmiNvvndg';
class YunMapBox {
  /**
   * 
   * @param {*} options 
   * el 挂在dom元素
   */
  constructor(options) {
    this.markers = {}
    this.layers = {}
    this.sources = {}
    this.initGaodeStyle()
    this.initMap(options)
    this.__bind()
  }

  //初始化地图图层
  // 高德-卫星图
  initGaodeStyle() {
    this.gaodeStyle = {
      version: 8,
      glyphs: "/glyphs/{fontstack}/{range}.pbf",
      sources: {
        "raster-tiles": {
          type: "raster",
          tiles: ["http://webst04.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=6&x={x}&y={y}&z={z}"],
          tileSize: 256
        },
        "raster-annotation": {
          type: "raster",
          tiles: ["http://webst02.is.autonavi.com/appmaptile?x={x}&y={y}&z={z}&lang=zh_cn&size=1&scale=1&style=8"],
          tileSize: 256
        }
      },
      layers: [{
        id: "simple-tiles",
        type: "raster",
        source: "raster-tiles",
        minzoom: 0,
        maxzoom: 22
      }, {
        id: "simple-annotation",
        type: "raster",
        source: "raster-annotation",
        minzoom: 0,
        maxzoom: 22
      }]
    };
  }
  // 高德街道图
  initGaodeStreeStyle() {
    this.gaodeStreeStyle = {
      version: 8,
      glyphs: "/glyphs/{fontstack}/{range}.pbf",
      sources: {
        "raster-tiles": {
          type: "raster",
          tiles: ["http://wprd04.is.autonavi.com/appmaptile?lang=zh_cn&size=1&style=7&x={x}&y={y}&z={z}"],
          tileSize: 256
        },
        // "raster-annotation": {
        //   type: "raster",
        //   tiles: ["http://webst02.is.autonavi.com/appmaptile?x={x}&y={y}&z={z}&lang=zh_cn&size=1&scale=1&style=8"],
        //   tileSize: 256
        // }
      },
      layers: [{
        id: "simple-tiles",
        type: "raster",
        source: "raster-tiles",
        minzoom: 0,
        maxzoom: 22
      },
        // {
        //   id: "simple-annotation",
        //   type: "raster",
        //   source: "raster-annotation",
        //   minzoom: 0,
        //   maxzoom: 22
        // }
      ]
    };
  }
  // 天地卫星图
  initTDStyle() {
    //实例化source对象
    var tdtVec = {
      //类型为栅格瓦片
      "type": "raster",
      'tiles': [
        "http://t0.tianditu.gov.cn/DataServer?T=img_w&x={x}&y={y}&l={z}&tk=4b01c1b56c6bcba2eb9b8e987529c44f"
      ],
      //分辨率
      'tileSize': 256
    };
    var tdtCva = {
      "type": "raster",
      'tiles': [
        "http://t1.tianditu.com/DataServer?T=cia_w&x={x}&y={y}&l={z}&tk=b9d4b98c977aee7408ab9c89def3aad7"
      ],
      'tileSize': 256
    };

    this.TDStyle = {
      //设置版本号，一定要设置
      "version": 8,
      glyphs: "https://fonts.mapbox.com/{fontstack}/{range}.pbf",
      //添加来源
      "sources": {
        "tdtVec": tdtVec,
        "tdtCva": tdtCva
      },
      "layers": [
        {
          //图层id，要保证唯一性
          "id": "tdtVec",
          //图层类型
          "type": "raster",
          //数据源
          "source": "tdtVec",
          //图层最小缩放级数
          "minzoom": 0,
          //图层最大缩放级数
          "maxzoom": 17
        },
        {
          "id": "tdtCva",
          "type": "raster",
          "source": "tdtCva",
          "minzoom": 0,
          "maxzoom": 17
        }
      ],
    }
  }
  //初始化地图

  initMap(options) {
    this.map = new mapbox.Map({
      container: options.el, // 设置地图容器
      style: this.gaodeStyle, // 应用自定义的地图样式
      center: options.center, // 设置地图的初始中心点
      zoom: options.zoom, // 设置地图的初始缩放级别
      pitch: options.pitch || 0, // 设置地图的俯仰角度，默认为0
      bearing: options.bearing || 0, // 设置地图的旋转角度，默认为0
      projection: options.projection || 'globe', // 设置地图的投影方式，默认为球面投影
      antialias: true, // 开启抗锯齿，提高地图渲染质量
      logoPosition: 'bottom-right', // 设置地图Logo的位置
      attributionControl: false, // 隐藏地图数据来源的控件
    })
  }
  /**
   * 创建maker
   * @param {*} option 
   * color:颜色
   * draggable：是否可以拖拽至新位置
   * className: 类名
   * element：dom元素
   * anchor: 锚点 'center','top','bottom','left','right','top-left','top-right','bottom-left','bottom-right'
   * offset:偏移量
   * rotation:旋转角度
   * rotationAlignment:旋转对齐方式
   * scale:缩放比例
   * pitchAlignment:俯仰对齐方式
   */
  addMarker(option) {
    const marker = new mapbox.Marker({
      color: option.color || '#ff0000',
      className: option.className || '',
      draggable: option.draggable || false,
      element: option.element || null,
      anchor: option.anchor || 'center',
      offset: option.offset || [0, 0],
      rotation: option.rotation || 0,
      rotationAlignment: option.rotationAlignment || 'auto',
      scale: option.scale || 1,
      pitchAlignment: option.pitchAlignment || 'auto'
    })
    marker.setLngLat(option.lngLat).addTo(this.map)
    marker.on('dragend', (e) => {
      option.onDragEnd && option.onDragEnd(e)
    })
    this.markers[option.id] = marker
  }
  // 移除marker
  removeMarker(id) {
    this.markers[id].remove()
    delete this.markers[id]
  }
  // 移除layer
  removeLayer(id) {
    this.map.removeLayer(id)
    this.map.removeSource(id)
    delete this.layers[id]
    delete this.sources[id]
  }
  // 设置标记的弹出窗口
  setMakerPupop(id, dom) {
    this.markers[id].setPopup(new mapbox.Popup({ offset: 25 }).setHTML(dom))
  }
  // 设置标记的弹出窗口-显隐，默认点击显示
  toggleMarkerPopup(id, flag) {
    this.markers[id].togglePopup(flag)
  }
  addGeojsonMarker(option) {
    if (!option.id) return
    if (this.layers[option.id]) this.removeLayer(option.id)

    this.map.loadImage(option.img, (error, image) => {
      if (error) throw error
      this.map.hasImage(option.id) && this.map.removeImage(option.id)
      this.map.addImage(option.id, image);
    })
    // 添加数据源
    let source = this.map.addSource(option.id, {
      type: 'geojson',
      data: option.data
    })
    //添加图层
    let layer = this.map.addLayer({
      id: option.id,
      type: 'symbol',
      source: option.id,
      slot:'top',
      maxzoom: option.maxzoom || 20,
      minzoom: option.minzoom || 0,
      metadata: {},
      paint: {
        'text-color': option.color || '#000',
        'text-halo-color': option.haloColor || '#fff',
        'text-halo-width': option.haloWidth || 1,
        'text-halo-blur': option.haloBlur || 0
      },
      layout: {
        'icon-ignore-placement': true,
        'icon-allow-overlap': true, //是否允许图标相互覆盖
        'icon-anchor': 'bottom', //图标的锚点
        'icon-color': option.color || '#000', //图标颜色
        'icon-image': option.id,
        'icon-size': option.iconSize,
        'icon-offset': [0, -10],
        'icon-opacity': option.opacity || 1,
        
        'symbol-placement': 'point',
        'symbol-z-elevate': true,
        'icon-optional': true, //是否允许图标超出边界
        'icon-text-fit': 'none',//图标大小
        // 'icon-rotation-alignment': 'map', //图标立起来控制
        'icon-rotate': option.rotate || 0, //图标旋转角度
        'text-allow-overlap': true, //是否允许文字相互覆盖
        'text-anchor': 'center', //文字的锚点
        'text-field': option.name || ['get', 'title'],
        'text-color': option.color || '#000', //文字颜色
        'text-font': ['Open Sans Semibold'],
        // 'text-rotation-alignment':'map'||'auto',
        'text-offset': [0, 0],
        'text-opacity': option.opacity || 1,
        'text-size': option.textSize || 12,
        'text-max-width': 10,
        'text-line-height': 1.2,
        'text-justify': 'center',
        'text-padding': 5,
      }
    })
    this.sources[option.id] = source
    this.layers[option.id] = layer
  }
  addImage(name,url) {
    url&&this.map.loadImage(url, (error, image) => {
      if (error) throw error
      this.map.hasImage(name) && this.map.removeImage(name)
      this.map.addImage(name, image);
    })
  }
  addCluster(option) {
    if (!option.id) return
    if (this.layers[option.id]) this.removeLayer(option.id)
    this.addImage(option.id,option.img)  
    this.addImage('cluster_'+option.id,option.cluterImg)
    // 添加数据源
    let source = this.map.addSource(option.id, {
      type: 'geojson',
      data: option.data,
      cluster: true,
      clusterRadius: 30,
    })
    //添加图层
    let layer = this.map.addLayer({
      id: option.id,
      type: 'symbol',
      source: option.id,
      maxzoom: option.maxzoom || 20,
      minzoom: option.minzoom || 0,
      metadata: {},
      paint: {
        'text-color': option.color || '#000',
        'text-halo-color': option.haloColor || '#fff',
        'text-halo-width': option.haloWidth || 1,
        'text-halo-blur': option.haloBlur || 0
      },
      layout: {
        'icon-ignore-placement': true,
        'icon-allow-overlap': true, //是否允许图标相互覆盖
        'icon-anchor': 'bottom', //图标的锚点
        'icon-color': option.color || '#000', //图标颜色
        'icon-image': [
          'case',
          ['==', ['get', 'cluster'], true],
          'cluster_'+ option.id,
          option.id
        ],
        'icon-size': option.iconSize,
        'icon-offset': [0, 0],
        'icon-opacity': option.opacity || 1,
        // 'icon-optional': true, //是否允许图标超出边界
        'icon-text-fit-padding': [2, 2, 2, 2],
        'text-allow-overlap': true, //是否允许文字相互覆盖
        'text-anchor': 'center', //文字的锚点
        'text-field': [
          'case',
          ['==', ['get', 'cluster'], true],
          ['concat',['get', 'point_count_abbreviated'] , '个'],
          ['get', 'title']
        ],
        'text-color': option.color || '#000', //文字颜色
        'text-font': ['Open Sans Semibold'],
        "text-offset": [
          'case',
          ['==', ['get', 'cluster'], true],
          [1.2,-1.5],
          [0,0]
          
        ],
        "text-size": [
          'case',
          ['==', ['get', 'cluster'], true],
          60,
          20
        ],
        'text-opacity': option.opacity || 1,
        'text-size': option.textSize || 12,
        // 'text-max-width': 10,
        'text-line-height': 1.2,
        'text-justify': 'center',
        'text-padding': 5,
      }
    })
    this.sources[option.id] = source
    this.layers[option.id] = layer
  }
  addLine(option) {
    if (!option.id) return
    if (this.layers[option.id]) this.removeLayer(option.id)
    // 添加数据源
    let source = this.map.addSource(option.id, {
      type: 'geojson',
      data: option.data
    })
    //添加图层
    let layer = this.map.addLayer({
      id: option.id,
      type: 'line',
      source: option.id,
      paint: {
        'line-color': option.color || '#000',
        'line-width': option.width || 1,
        'line-opacity': option.opacity || 1,
        'line-dasharray': option.dash || [1, 0],
        'line-emissive-strength': option.emissive || 0,
        'line-gap-width': option.gapWidth || 0,
      },
      layout: {
        'line-join': 'round',
        'line-cap': 'round',
        'line-blur': 0.5,
        'line-miter-limit': 2,
        'line-offset': 0,
        'line-pattern': 'line-pattern',
      }
    })
    this.sources[option.id] = source
    this.layers[option.id] = layer
  }
  addColorLine(option) {
    if (!option.id) return
    if (this.layers[option.id]) this.removeLayer(option.id)
    // 添加数据源
    let source = this.map.addSource(option.id, {
      type: 'geojson',
      lineMetrics: true,
      data: option.data
    })
    //添加图层
    let layer = this.map.addLayer({
      id: option.id,
      type: 'line',
      source: option.id,
      paint: {
        'line-color': option.color || '#000',
        'line-width': option.width || 1,
        'line-opacity': option.opacity || 1,
        'line-dasharray': option.dash || [1, 0],
        'line-emissive-strength': option.emissive || 0,
        'line-gap-width': option.gapWidth || 0,
        'line-gradient': option.gradient || [0, 0, 0, 0, 0, 0, 0, 0],
      },
      layout: {
        'line-join': 'round',
        'line-cap': 'round',
        'line-blur': 0.5,
        'line-miter-limit': 2,
        'line-offset': 0,
        'line-pattern': 'line-pattern',
      }
    })
    this.sources[option.id] = source
    this.layers[option.id] = layer
  }
  addStreamline(option) {
    if (!option.id) return
    if (this.layers[option.id]) this.removeLayer(option.id)
    if (this.layers[option.bg_id]) this.removeLayer(option.bg_id)
    // 添加数据源
    let source = this.map.addSource(option.id, {
      type: 'geojson',
      data: option.data
    })
    //添加图层
    let bglayer = this.map.addLayer({
      id: option.bg_id,
      type: 'line',
      source: option.id,
      paint: {
        'line-color': option.bg_color || '#000',
        'line-width': option.width || 1,
        'line-opacity': option.opacity || 1,
      },
      layout: {
        'line-join': 'round',
        'line-cap': 'round',
      }
    })
    //添加图层
    let layer = this.map.addLayer({
      id: option.id,
      type: 'line',
      source: option.id,
      paint: {
        'line-color': option.color || '#000',
        'line-width': option.width || 1,
        'line-opacity': 1,
        'line-dasharray': option.dash || [0, 4, 2],
      },
      layout: {
        // 'line-join': 'round',
        // 'line-cap': 'round',
      }
    })
    this.sources[option.id] = source
    this.layers[option.id] = layer
    this.layers[option.bg_id] = bglayer
    let dashArraySequence = [
      [0, 4, 3],
      [0.5, 4, 2.5],
      [1, 4, 2],
      [1.5, 4, 1.5],
      [2, 4, 1],
      [2.5, 4, 0.5],
      [3, 4, 0],
      [0, 0.5, 3, 3.5],
      [0, 1, 3, 3],
      [0, 1.5, 3, 2.5],
      [0, 2, 3, 2],
      [0, 2.5, 3, 1.5],
      [0, 3, 3, 1],
      [0, 3.5, 3, 0.5]
    ]
    let step = 0;
    const animateDashArray = (timestamp)=> {
      const newStep = parseInt(
          (timestamp / 20) % dashArraySequence.length
      );
      if (newStep !== step) {
          this.map.setPaintProperty(
              option.id,
              'line-dasharray',
              dashArraySequence[step]
          );
          step = newStep;
      }
      requestAnimationFrame(animateDashArray);
    }
    animateDashArray(0)
  }
  addPolygon(option) {
    if (!option.id) return
    if (this.layers[option.id]) this.removeLayer(option.id)
    option.img&&this.map.loadImage(option.img, (error, image) => {
      if (error) throw error
      this.map.hasImage(option.id) && this.map.removeImage(option.id)
      this.map.addImage(option.id, image);
    })
    let source = this.map.addSource(option.id, {
      type: 'geojson',
      data: option.data
    })
    let layerOption = {
      id: option.id,
      type: 'fill',
      source: option.id,
      paint: {
        'fill-color': option.fillColor || '#0080ff',
        'fill-opacity': option.opacity || 0.8,
        'fill-outline-color': option.outlineColor || '#000',
      },
    }
    if(option.img){
      layerOption.paint['fill-pattern'] = option.id
    }
    let layer = this.map.addLayer(layerOption)
    this.sources[option.id] = source
    this.layers[option.id] = layer
  }
  addCircle(option){
    let source = this.map.addSource(option.id, {
      type: 'geojson',
      data: option.data,
    })
    let layerOption = {
      id: option.id,
      type: 'circle',
      source: option.id,
      paint: {
        'circle-color': option.color || '#fff',
        'circle-radius': option.radius || 5,
        'circle-stroke-color': option.outlineColor || '#000',
        'circle-stroke-width': option.outlineWidth || 0,
     },
    }
    let layer = this.map.addLayer(layerOption)
    this.sources[option.id] = source
    this.layers[option.id] = layer
  }
  addfillExtrusion(option) {
    if (!option.id) return
    if (this.layers[option.id]) this.removeLayer(option.id)
    option.img&&this.map.loadImage(option.img, (error, image) => {
      if (error) throw error
      this.map.hasImage(option.id) && this.map.removeImage(option.id)
      this.map.addImage(option.id, image);
    })
    let source = this.map.addSource(option.id, {
      type: 'geojson',
      data: option.data,
    })
    let layerOption = {
      id: option.id,
      type: 'fill-extrusion',
      source: option.id,
      paint: {
        'fill-extrusion-color': option.color || '#000',
        'fill-extrusion-opacity': option.opacity || 1,
        'fill-extrusion-height': option.height || 0,
        'fill-extrusion-base': option.base || 0,
        'fill-extrusion-ambient-occlusion-ground-attenuation':1,
        'fill-extrusion-ambient-occlusion-ground-radius':100,
        'fill-extrusion-ambient-occlusion-intensity':1,
        'fill-extrusion-ambient-occlusion-wall-radius':100,
        'fill-extrusion-flood-light-color':'#fff',
        'fill-extrusion-flood-light-ground-radius':100,
        'fill-extrusion-flood-light-wall-radius':100,
      }
    }
    if(option.img){
      layerOption.paint['fill-extrusion-pattern'] = option.id
    }
    let layer = this.map.addLayer(layerOption)
    this.layers[option.id] = layer
    this.sources[option.id] = source
  }
  addHeatmap(option){
    if (!option.id) return
    if (this.layers[option.id]) this.removeLayer(option.id)
    let source = this.map.addSource(option.id, {
      type: 'geojson',
      data: option.data,
    })
    let layerOption = {
      id: option.id,
      type: 'heatmap',
      source: option.id,
      paint: {
        'heatmap-radius': option.radius || [
          'interpolate',
          ['linear'],
          ['zoom'],
          0,
          10,
          20,
          100
      ],
        'heatmap-weight': option.weight || ['interpolate', ['linear'], ['get', 'mag'], 0, 0, 10, 1],
        'heatmap-intensity': option.intensity || [
          'interpolate',
          ['linear'],
          ['zoom'],
          0,
          1,
          9,
          3
      ],
        'heatmap-opacity': option.opacity ||  [
          'interpolate',
          ['linear'],
          ['zoom'],
          18,
          1,
          20,
          0
      ],
        'heatmap-color': option.color || [
          'interpolate',
          ['linear'],
          ['heatmap-density'],
          0.1,
          'rgba(0, 0, 255,0)',
          0.5,
          'rgb(255, 185, 0)',
          0.8,
         'rgb(255, 0, 0)',
        ]
      }
    }
    let layer = this.map.addLayer(layerOption)
    this.layers[option.id] = layer
    this.sources[option.id] = source
  }
  __bind() {
    const methods = Object.getOwnPropertyNames(Object.getPrototypeOf(this))
    methods.forEach(name => {
      if (typeof this[name] === 'function') {
        this[name] = this[name].bind(this)
      }
    })
  }
}
export default YunMapBox