
import jsPDF from 'jspdf';
import 'ol/ol.css'
import Map from 'ol/Map'
//视图
import View from 'ol/View'
//图层
import TileLayer from 'ol/layer/Tile'
import { Vector as layerVector } from 'ol/layer'
// 矢量数据源
import { Vector as sourceVector } from 'ol/source'
import OSM from 'ol/source/OSM'
import SourceXYZ from 'ol/source/XYZ'
import { Style, Circle as styleCircle, Fill, Stroke, Icon } from 'ol/style'
import { LineString, Point,Circle,Polygon} from 'ol/geom'
import {fromCircle,fromExtent} from 'ol/geom/Polygon'
import { Feature, } from 'ol'
//数据处理
import GeoJSON from "ol/format/GeoJSON"
//控制器
import ZoomToExtent from 'ol/control/ZoomToExtent'
import ZoomSlider from 'ol/control/ZoomSlider'
import FullScreen from 'ol/control/FullScreen'
import { OverviewMap, defaults as defaultControls } from 'ol/control.js';
class YunOlMap {
  /**
   * 
   * @param {*} options 
   * el 挂在dom元素
   */
  constructor(options) {
    this.initMap(options)
    this.__bind()
  }
  //初始化图层
  initLayers() {
    // 天地图
    let satelliteLayer = new TileLayer({
      source: new SourceXYZ({
        visible: true,
        url: `http://t0.tianditu.gov.cn/DataServer?T=img_w&x={x}&y={y}&l={z}&tk=4b01c1b56c6bcba2eb9b8e987529c44f`,//卫星图
        crossOrigin: "anonymous"
      })
    })
    let satelliteLabelLayer = new TileLayer({
      source: new SourceXYZ({
        visible: false,
        url: `http://t1.tianditu.com/DataServer?T=cia_w&x={x}&y={y}&l={z}&tk=b9d4b98c977aee7408ab9c89def3aad7`,//卫星标注图
        crossOrigin: "anonymous"
      })
    })
    let streetLayer = new TileLayer({
      source: new SourceXYZ({
        visible: false,
        url: 'http://t{0-7}.tianditu.com/DataServer?T=vec_w&x={x}&y={y}&l={z}&tk=7786923a385369346d56b966bb6ad62f',//天地图街道图
        crossOrigin: "anonymous"
      })
    })
    let streetLabelLayer = new TileLayer({
      source: new SourceXYZ({
        visible: false,
        url: 'http://t{0-7}.tianditu.com/DataServer?T=cva_w&x={x}&y={y}&l={z}&tk=7786923a385369346d56b966bb6ad62f',//天地图街道图注记
        crossOrigin: "anonymous"
      })
    })
    let terLayer = new TileLayer({
      source: new SourceXYZ({
        visible: false,
        url: 'http://t{0-7}.tianditu.com/DataServer?T=ter_w&x={x}&y={y}&l={z}&tk=7786923a385369346d56b966bb6ad62f',//天地图地形
        crossOrigin: "anonymous"
      })
    })
    const osmLayer = new TileLayer({
      visible: false,
      source: new OSM()
    })
    //高德影像
    const gaodeImgLayer = new TileLayer({
      visible: false,
      source: new SourceXYZ({
        url: `http://webst0{1-4}.is.autonavi.com/appmaptile?style=6&x={x}&y={y}&z={z}`,
        crossOrigin: "anonymous"
      })
    })
    //高德街道
    const gaodeWprdLayer = new TileLayer({
      visible: false,
      source: new SourceXYZ({
        url: `https://wprd0{1-4}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&style=7&x={x}&y={y}&z={z}`,
        crossOrigin: "anonymous"
      })
    })

    //高德街道 
    const gaodeVecLayer = new TileLayer({
      visible: false,
      source: new SourceXYZ({
        url: `http://webrd0{1-4}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=7&x={x}&y={y}&z={z}`,
        crossOrigin: "anonymous"
      })
    })
    //arcgis影像 
    const arcgisImgLayer = new TileLayer({
      visible: false,
      source: new SourceXYZ({
        url: 'https://server.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
        crossOrigin: "anonymous"
      })
    });
    //arcgis街道 
    const arcgisVecLayer = new TileLayer({//arcgis街道 
      visible: false,
      source: new SourceXYZ({
        url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
        crossOrigin: "anonymous"
      })
    });
    //谷歌影像 
    const googleImgLayer = new TileLayer({//谷歌影像 
      visible: false,
      source: new SourceXYZ({
        url: 'http://www.google.cn/maps/vt?lyrs=s@189&gl=cn&x={x}&y={y}&z={z}',
        crossOrigin: "anonymous"
      })
    });
    //谷歌街道 
    const googleVecLayer = new TileLayer({//谷歌街道 
      visible: false,
      source: new SourceXYZ({
        url: 'http://www.google.cn/maps/vt?lyrs=m@189&gl=cn&x={x}&y={y}&z={z}',
        crossOrigin: "anonymous"
      })
    });
    //谷歌-影像标注
    const googleHLayer = new TileLayer({
      visible: false,
      source: new SourceXYZ({
        url: 'http://www.google.cn/maps/vt?lyrs=h@189&gl=cn&x={x}&y={y}&z={z}',
        crossOrigin: "anonymous"
      })
    });
    //谷歌-地形图
    const googleTLayer = new TileLayer({
      visible: false,
      source: new SourceXYZ({
        url: 'http://www.google.cn/maps/vt?lyrs=t@189&gl=cn&x={x}&y={y}&z={z}',
        crossOrigin: "anonymous"
      })
    });
    const layers = [
      satelliteLayer,
      satelliteLabelLayer,
      streetLayer,
      streetLabelLayer,
      terLayer,
      osmLayer,
      gaodeImgLayer,
      gaodeVecLayer,
      gaodeWprdLayer,
      arcgisImgLayer,
      arcgisVecLayer,
      googleImgLayer,
      googleVecLayer,
      googleHLayer,
      googleTLayer
    ]
    this.layers = layers
    this.layersObj = {
      "天地图-卫星图": satelliteLayer,
      "天地图-街道图": streetLayer,
      "天地图-街道标注图": satelliteLabelLayer,
      "天地图-标注图": streetLabelLayer,
      "天地图-地形图": terLayer,
      "OSM": osmLayer,
      "高德-卫星图": gaodeImgLayer,
      "高德-街道图1": gaodeWprdLayer,
      "高德-街道图2": gaodeVecLayer,
      "arcgis-卫星图": arcgisImgLayer,
      "arcgis-街道图": arcgisVecLayer,
      "谷歌-卫星图": googleImgLayer,
      "谷歌-卫星标注图": googleHLayer,
      "谷歌-街道图": googleVecLayer,
      "谷歌-地形图": googleTLayer,
    }
  }
  //获取所有图层的名称
  getLayersLabel() {
    let layersLabel = Object.keys(this.layersObj)
    return layersLabel
  }
  //切换图层
  changeLayers(layer) {
    this.layers.forEach(layer => {
      layer.setVisible(false)
    })
    this.layersObj[layer].setVisible(true)
  }
  /**
  * 初始化地图
  */
  initMap(options) {
    this.initLayers()
    const YunMap = new Map({
      target: options.el,
      layers: this.layers,
      view: new View({  //显示范围
        center: options.center || [116.402616, 39.929709], //中心点
        zoom: options.zoom || 15, //缩放层级
        projection: "EPSG:4326" //坐标系
      })
    })
    this.Map = YunMap
  }
  /**
   * 视图跳转控件
   *
   */
  zoomToExtent() {
    const zoomToExtent = new ZoomToExtent({
      extent: [110, 30, 150, 30]
    })
    this.Map.addControl(zoomToExtent)
  }
  /**
   * 图层放大缩小
   */
  zoomSlider() {
    const zoomSlider = new ZoomSlider()
    this.Map.addControl(zoomSlider)
  }
  /**
   * 全屏
   */
  fullScreen() {
    const fullScreen = new FullScreen()
    this.Map.addControl(fullScreen)
  }
  /**
   * 鹰眼地图
   */
  addOverview(layerName) {
    const gaodeVecLayer = new TileLayer({
      source: new SourceXYZ({
        url: `http://webrd0{1-4}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=7&x={x}&y={y}&z={z}`,
        crossOrigin: "anonymous"
      })
    })
    const overviewMapControl = new OverviewMap({
      className: 'ol-overviewmap ol-custom-overviewmap',
      layers: [gaodeVecLayer],
      collapseLabel: '\u00BB',
      label: '\u00AB',
      collapsed: false,
    });
    this.Map.addControl(overviewMapControl)
  }
  /**
   * 获取地图窗口范围
   */
  getMapViewBound() {
    let view = this.Map.getView()
    return view.calculateExtent(this.Map.getSize())
  }
  /**
   * 导出地图图片
   */
  exportMapImg() {
    let _this = this
    this.Map.once('postcompose', function (event) {
      const mapCanvas = document.createElement('canvas');
      const size = _this.Map.getSize();
      mapCanvas.width = size[0]
      mapCanvas.height = size[1]
      const mapContext = mapCanvas.getContext('2d');
      Array.prototype.forEach.call(
        document.querySelectorAll('.ol-layers canvas'),
        function (canvas) {
          if (canvas.width > 0) {
            const opacity = canvas.parentNode.style.opacity;
            mapContext.globalAlpha = opacity === '' ? 1 : Number(opacity);
            const transform = canvas.style.transform;
            // Get the transform parameters from the style's transform matrix
            const transformTemp = transform
              .match(/^matrix\(([^\\(]*)\)$/);

            const matrix = transformTemp && transformTemp[1]
              .split(',')
              .map(Number);
            // Apply the transform to the export map context
            CanvasRenderingContext2D.prototype.setTransform.apply(
              mapContext,
              matrix
            );
            mapContext.drawImage(canvas, 0, 0);
          }
        }
      );
      if (navigator.msSaveBlob) {
        navigator.msSaveBlob(mapCanvas.msToBlob(), 'map.png');
      } else {
        mapCanvas.toBlob(function (blob) {
          _this.saveAs(blob, 'map.png');
        });
      }
    });
    this.Map.renderSync();
  }
  /**
   * 导出地图为PDF
   */
  exportMapPdf() {
    let _this = this
    this.Map.once('postcompose', function (event) {
      const mapCanvas = document.createElement('canvas');
      const size = [297, 210];
      const mapSize = _this.Map.getSize();
      console.log(size,mapSize)
      mapCanvas.width = Math.round((size[0] * 150) / 25.4)
      mapCanvas.height = Math.round((size[1] * 150) / 25.4)
      const mapContext = mapCanvas.getContext('2d');
      Array.prototype.forEach.call(
        document.querySelectorAll('.ol-layers canvas'),
        function (canvas) {
          if (canvas.width > 0) {
            const opacity = canvas.parentNode.style.opacity;
            mapContext.globalAlpha = opacity === '' ? 1 : Number(opacity);
            const transform = canvas.style.transform;
            // Get the transform parameters from the style's transform matrix
            const transformTemp = transform
              .match(/^matrix\(([^\\(]*)\)$/);

            const matrix = transformTemp && transformTemp[1]
              .split(',')
              .map(Number);
            // Apply the transform to the export map context
            CanvasRenderingContext2D.prototype.setTransform.apply(
              mapContext,
              matrix
            );
            mapContext.drawImage(canvas, 0, 0);
          }
        }
      );
      mapContext.globalAlpha = 1;
      mapContext.setTransform(1, 0, 0, 1, 0, 0);
      const pdf = new jsPDF('landscape', undefined, 'a4');
      
      pdf.addImage(
        mapCanvas.toDataURL('image/jpeg'),
        'JPEG',
        0,
        0,
        size[0],
        size[1]
      );
      pdf.save('map.pdf');
    });
    this.Map.renderSync();
  }
  /**
   * 保存文件
   * @param {*} blob 
   * @param {*} fileName 
   */
  saveAs(blob, fileName) {
    // 创建用于下载文件的a标签
    const d = document.createElement('a')
    // 设置下载内容
    d.href = window.URL.createObjectURL(blob)
    // 设置下载文件的名字
    d.download = fileName
    // 界面上隐藏该按钮
    d.style.display = 'none'
    // 放到页面上
    document.body.appendChild(d)
    // 点击下载文件
    d.click()
    // 从页面移除掉
    document.body.removeChild(d)
    // 释放 URL.createObjectURL() 创建的 URL 对象
    window.URL.revokeObjectURL(d.href)
  }
  /**
  * 获取转换后单位的半径
  * @param {Number} radius 以米为单位的半径的值
  * @returns {Number} circleRadius 以投影的单位为单位的半径的值
  */
  getRadius(radius){
    let metersPerUnit = this.Map.getView().getProjection().getMetersPerUnit();
    let circleRadius =  radius / metersPerUnit;
    return circleRadius;
  }
  /**
   * 添加maker点
   */
  addMaker(option) {
    let { lngLat, radius, icon, animation, PfillColor, PstrokeWidth, PstrokeColor } = option
    //要素
    const point = new Feature({
      geometry: new Point(lngLat)
    })
    //样式
    let style = new Style({
      //设置点样式
      image: icon ? new Icon({
        anchor: [0.5, 1],
        img: icon.img,
        src: icon.src,
        width: icon.width,
        height: icon.height,
        scale: icon.scale,
        opacity: icon.opacity
      }) : new styleCircle({
        radius: radius || 10, //设置点的半径 单位degree
        fill: new Fill({ color: PfillColor || "#ff2d51" }),
        stroke: new Stroke({
          width: PstrokeWidth || 2,
          color: PstrokeColor || "yellow"
        })
      })
    })
    point.setStyle([
      style
    ])
    // 要素添加到矢量数据源
    let source = new sourceVector({
      features: [point]
    })
    // 矢量数据源添加到矢量图层
    let layer = new layerVector({
      source: source
    })
    // 矢量图层添加到地图容器
    this.Map.addLayer(layer)
    if (animation) {
      let max = animation.max
      let min = animation.min
      let min2 = animation.min
      layer.on('postrender', () => {
        if (min > max) {
          min = animation.min
        }
        if (min2 > max) {
          min2 = animation.min
        }
        point.setStyle([
          new Style({
            image: new styleCircle({
              radius: min, //设置点的半径 单位degree
              fill: new Fill({ color: `rgba(255,0,47,${6 / min})` }),
            })
          }),
          new Style({
            image: new styleCircle({
              radius: min2, //设置点的半径 单位degree
              stroke: new Stroke({
                color: `rgba(255,0,47,${6 / min2})`
              }),
              width: 1
            })
          }),
          style
        ])
        min += 0.6
        min2 += 0.4
      })
      this.Map.render()
    }

  }
  /**
   * 添加线
   * @param {*} option 
   */
  addLine(option){
    let {lngLat,lineColor,lineWidth} = option
    let line = new Feature({
      geometry:new LineString(lngLat)
    })
    let style = new Style({
      stroke:new Stroke({
        width:lineWidth||2,
        color:lineColor||'yellow'
      })
    })
    line.setStyle(style)
    let source = new sourceVector({
      features:[line]
    })
    let layer = new layerVector({
      source:source
    })
    this.Map.addLayer(layer)
  }
  /**
   * 添加圆
   * @param {*} option 
   */
  addCircle(option){
    let {center,radius,strokeColor,strokeWidth,fillColor} = option
    let circle = new Feature({
      geometry:new Circle(center,this.getRadius(radius))
    })
    let style = new Style({
      stroke:new Stroke({
        width:strokeWidth||2,
        color:strokeColor||'yellow'
      }),
      fill:new Fill({
        color:fillColor||'rgba(255, 255, 255, 0.5)'
      })
    })
    circle.setStyle(style)
    let source = new sourceVector({
      features:[circle]
    })
    let layer = new layerVector({
      source:source
    })
    this.Map.addLayer(layer)
  }
  /**
   * 添加正方形
   * @param {*} option 
   */
  addSquare(option){
    let {center,radius,strokeColor,strokeWidth,fillColor} = option
    let circle = new Circle(center,this.getRadius(radius))
    let square = new Feature({
      geometry: fromCircle(circle,4,150)
    })
    let style = new Style({
      stroke:new Stroke({
        width:strokeWidth||2,
        color:strokeColor||'yellow'
      }),
      fill:new Fill({
        color:fillColor||'rgba(255, 255, 255, 0.5)'
      })
    })
    square.setStyle(style)
    let source = new sourceVector({
      features:[square]
    })
    let layer = new layerVector({
      source:source
    })
    this.Map.addLayer(layer)
  }
  /**
   * 添加矩形
   * @param {*} option 
   */
  addRect(option){
    let {lngLat,strokeColor,strokeWidth,fillColor} = option
    let Rectangle = new Feature({
      geometry: fromExtent(lngLat)
    })
    let style = new Style({
      stroke:new Stroke({
        width:strokeWidth||2,
        color:strokeColor||'yellow'
      }),
      fill:new Fill({
        color:fillColor||'rgba(255, 255, 255, 0.5)'
      })
    })
    Rectangle.setStyle(style)
    let source = new sourceVector({
      features:[Rectangle]
    })
    let layer = new layerVector({
      source:source
    })
    this.Map.addLayer(layer)
  }
   /**
   * 添加多边形
   * @param {*} option 
   */
   addPolygon(option){
    let {lngLat,strokeColor,strokeWidth,fillColor} = option
    let polygon = new Feature({
      geometry: new Polygon(lngLat)
    })
    let style = new Style({
      stroke:new Stroke({
        width:strokeWidth||2,
        color:strokeColor||'yellow'
      }),
      fill:new Fill({
        color:fillColor||'rgba(255, 255, 255, 0.5)'
      })
    })
    polygon.setStyle(style)
    let source = new sourceVector({
      features:[polygon]
    })
    let layer = new layerVector({
      source:source
    })
    this.Map.addLayer(layer)
  }
  /**
   * 区域遮罩
   */
  addAreaMask(option){
    let {lngLat,strokeColor,strokeWidth,fillColor} = option
    let linearRing = new Polygon(lngLat);
    // let extent = this.Map.getView().calculateExtent();
    // console.log(extent)
    let extent = [-180, -90, 180, 90];
    //针对视窗范围设置Extent
    let polygonRing = fromExtent(extent);
    //把视窗范围添加至边界线中也就是确定外环位置
    polygonRing.appendLinearRing(linearRing);
    //把数据生成Feature
    let Polygons = new Feature({
      geometry:polygonRing
    });
    //实例化一个矢量图层Vector作为绘制层
    let vectorSource = new sourceVector({
      features: [Polygons],
      // features: geoJson.readFeatures(area),
    });

    //创建一个图层并设置填充样式
    let vector = new layerVector({
      source: vectorSource,
      style: new Style({
        fill: new Fill({
          color: fillColor||'rgba(255,255,255,0.7)'
        }),
        stroke: new Stroke({
          lineDash: [1, 2, 3, 4, 5],
          color: strokeColor||'#ffcc33',
          width: strokeWidth||4,
        }),
      })
    });
    //设置图层层级
    vector.setZIndex(0);
    //添加至地图
    this.Map.addLayer(vector);
  }
  /**
   * 添加geojson数据
   */
  addGeoJson(option) {
    let source = new sourceVector({
      features: new GeoJSON().readFeatures(option.geojson)
    })
    let layer = new layerVector({
      source: source
    })
    //样式
    let style = new Style({
      //设置点样式
      image: new styleCircle({
        radius: 10, //设置点的半径 单位degree
        fill: new Fill({ color: option.pointFill || "#ff2d51" }),
        stroke: new Stroke({
          width: option.pointStrokeWidth || 2,
          color: option.pointStrokeColor || "yellow"
        })
      }),
      stroke: new Stroke({
        color: option.strokeColor || '#ff2d51',
        width: option.strokeWidth || 2
      }),
      fill: new Fill({
        color: option.fillColor || 'rgba(200,200,230,0.2)'
      })
    })
    layer.setStyle(style)
    this.Map.addLayer(layer)
  }
  /**
   * 添加geojson数据
   */
  addGeoJsonFile(option) {
    let source = new sourceVector({
      url: option.fileurl,
      format: new GeoJSON()
    })
    let layer = new layerVector({
      source: source
    })
    //样式
    let style = new Style({
      //设置点样式
      image: new styleCircle({
        radius: 10, //设置点的半径 单位degree
        fill: new Fill({ color: option.pointFill || "#ff2d51" }),
        stroke: new Stroke({
          width: option.pointStrokeWidth || 2,
          color: option.pointStrokeColor || "yellow"
        })
      }),
      stroke: new Stroke({
        color: option.strokeColor || '#ff2d51',
        width: option.strokeWidth || 2
      }),
      fill: new Fill({
        color: option.fillColor || 'rgba(200,200,230,0.2)'
      })
    })
    layer.setStyle(style)
    this.Map.addLayer(layer)
  }
  /**
   * 绑定事件
   * @param {*} eventName 事件名称
   * @param {*} fn 回调函数
   */
  on(eventName, fn) {
    this.Map.on(eventName, fn)
  }
  /**
   * 地图窗口移动
   */
  flyTo(option) {
    let { center, zoom, duration } = option
    const view = this.Map.getView()
    view.animate({
      center: center,
      zoom: zoom,
      duration: duration || 1500
    })
  }
  __bind() {
    const methods = Object.getOwnPropertyNames(Object.getPrototypeOf(this))
    methods.forEach(name => {
      if (typeof this[name] === 'function') {
        this[name] = this[name].bind(this)
      }
    })
  }
}
export default YunOlMap