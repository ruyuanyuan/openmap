# Openlayer

## Map 地图

## Layers 图层

## Source 数据源

## Feature 矢量图形对象

### geometry

geometry属性表示Feature的空间位置，它可以是点（Point）、线（LineString）、多边形（Polygon）或集合（MultiPoint、MultiLineString、MultiPolygon）。在使用时，需要根据实际情况选择合适的geometry

```
//创建一个点的Feature

vectorLayer.getSource().addFeature(
    new Feature({
      geometry: new Point([125.35, 43.88]),
    })
)


//创建一个线的Feature

vectorLayer.getSource().addFeature(
    // 添加线图层
    new Feature({
      geometry: new LineString([
        [125.3579180563, 43.888298024],
        [125.3587389704, 43.887798338],
      ]),
    })
)

//创建一个面的Feature

vectorLayer.getSource().addFeature(
    // 添加面图层
    new Feature({
      geometry: new Polygon([
        [
          [125.3579180563, 43.888298024],
          [125.3587389704, 43.887798338],
          [125.3574397847, 43.8865062907],
          [125.3579180563, 43.888298024],
        ],
      ]),
    })
  )



```



### attributes